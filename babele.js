Hooks.once('init', () => {

    if (typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'aos-zh-tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });

        Babele.get().registerConverters({
            "npc-item-translation": NPCTranslator.item(),       
        });
    }
});

class NPCTranslator {
    static item() {
        let itemMapping = {
            name: "name",
            description: "system.description"
        };
        let dynamicMapping = new CompendiumMapping('Item', itemMapping);
        return function(items, translations) {
            return items.map(data => {

                if(translations) {

                    let translation;
                    if(Array.isArray(translations)) {
                        translation = translations.find(t => t.id === data._id || t.id === data.name);
                    } else {
                        translation = translations[data._id] || translations[data.name];
                    }
                    if(translation) {
                        let translatedData = dynamicMapping.map(data, translation);
                        return mergeObject(data, mergeObject(translatedData, { translated: true }));
                    }
                }

                let pack = game.babele.packs.find(pack => pack.translated && pack.hasTranslation(data));
                if(pack) {
                    return pack.translate(data);
                }
                return data;
            });
        }
    }
}